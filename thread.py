import json, urllib.request

class Thread:
    def __init__(self):
        self._url = ""
        self._thread_name = ""
        self._board = ""
        self._thread_id = 0
        self._posts = []
    
    def getThreadName(self) -> str:
        return self._thread_name
    
    def getThreadID(self) -> int:
        return self._thread_id

    def setURL(self,url:str):
        self._url = url
    
    def getPosts(self) -> list:
        self.__getJSON()
        return self._posts
    
    def getBoard(self) -> str:
        return self._board
    
    def getURL(self) -> str:
        return self._url

    def reset(self):
        self._url = ""
        self._thread_name = ""
        self._board = ""
        self._thread_id = 0
        self._posts = []

    def __getJSON(self):
        #url form: http(s)://boards.4chan(nel).org/[board]/thread/[number]
        #json url: http(s):a.4cdn.org/[board]/thread/[number].json
        slash_index = self._url.rfind("4channel")
        slash_index_skip = 13

        if(slash_index == -1):
            slash_index = self._url.rfind("4chan")
            slash_index_skip = 10

        end_index = self._url.rfind("thread")
        self._board = self._url[slash_index + slash_index_skip:end_index-1]
        self._thread_id = self._url[end_index+7:]
        json_url = "http://a.4cdn.org/{}/thread/{}.json".format(self._board,self._thread_id)
        print(self._board)
        self.__downloadJSON(json_url)

    def __downloadJSON(self,url:str):
        posts = json.loads(urllib.request.urlopen(url).read().decode('utf8'))
        
        if('sub' in posts['posts'][0]):
            self._thread_name = posts['posts'][0]['sub']
        else:
            self._thread_name = posts['posts'][0]['semantic_url']
        self.__parseJSON(posts['posts'])

    def __parseJSON(self, posts:list):
        for post in posts:
            filename = ""
            file_ext = ""
            comment = ""
            tim = 0
            if('filename' in post):
                filename = post['filename']
            if('ext' in post):
                file_ext = post['ext']
            if('com' in post):
                comment = post['com']
            if('tim' in post):
                tim = post['tim']
            
            p = Post(filename = filename, file_ext = file_ext, comment = comment, tim = tim, board = self._board)
            self._posts.append(p)

class Post:
    def __init__(self,filename:str = "", file_ext:str = "", comment:str = "", tim:int = 0,board:str = "" ):
        self.filename = filename
        self.file_ext = file_ext
        self.comment = comment
        self.tim = tim
        self.board = board