#!/usr/bin/env python3
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askdirectory
from tkinter import messagebox
from thread import Thread
import downloader,os,errno


main_window = Tk()
style = ttk.Style()
style.theme_use("clam")
main_window.title("imageboard-dl")
main_window.resizable(False,False)

thread = Thread()

def askDirectory():
    directory_var.set(askdirectory())
    
def downloadHelp(loc,posts):
    if(downloader.downloadFiles(loc,posts) == 1):
        messagebox.showerror("Error","Ran out of storage at save location")
    else:
        messagebox.showinfo("Done","All images were downloaded")

def startDownload():
    if(thread_entry.get().replace(" ","") == "" or "4chan.org" not in thread_entry.get()):
        messagebox.showerror("Error","Please enter a valid thread")
    elif(directory_entry.get().replace(" ","") == ""):
        messagebox.showerror("Error","Please enter a save location")
    else:
        url = thread_entry.get().strip()
        thread.setURL(url)
        posts = thread.getPosts()
        loc = directory_entry.get()
        if(loc == "/"):
            loc = loc + thread.getThreadName()
        else:
            if loc.endswith("/"):
                loc = directory_entry.get()+thread.getThreadName()
            else:
                loc = directory_entry.get()+"/"+thread.getThreadName()

        try:
            os.makedirs(loc)
            print("Folder created at {}".format(loc))
            downloadHelp(loc,posts)
        except FileExistsError:
            print("Folder already exists at {}. Saving files to it".format(loc))
            downloadHelp(loc,posts) 
        except PermissionError:
            messagebox.showerror("Error","Don't have permission to save files to location")
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
            else:
                print("Folder already exists at {}. Saving files to it".format(loc))
                downloadHelp(loc,posts)       
            
        thread.reset()

#frames
top_frame = ttk.Frame(main_window)
bottom_frame = ttk.Frame(main_window)
top_frame.pack(expand = True,fill = 'both')
bottom_frame.pack(side = "bottom", expand = True, fill = 'both')

#thread download
thread_label = ttk.Label(top_frame, text = "Thread")
download_button = ttk.Button(top_frame, text = "Download", command = startDownload)
thread_entry = ttk.Entry(top_frame)

thread_label.pack(side = "left")
download_button.pack(side = "right",padx = 5, pady = 5)
thread_entry.pack(side = "right", padx = 5)

#Directory
directory_var = StringVar()
directory_label = ttk.Label(bottom_frame, text = "Directory")
directory_button = ttk.Button(bottom_frame,text = "Select", command = askDirectory)
directory_entry = ttk.Entry(bottom_frame,textvariable = directory_var)

directory_label.pack(side = "left")
directory_button.pack(side = "right", padx = 5, pady = 5)
directory_entry.pack(side = "right", padx = 5)

main_window.mainloop()
