import asyncio,aiohttp
import os
from contextlib import closing
#based off of https://gist.github.com/Hammer2900/2b5da5c08f6406ab49ddb02b0c5ae9f7

def downloadFiles(directory:str,posts:list) -> int:
    asyncio.set_event_loop(asyncio.new_event_loop())
    with closing(asyncio.get_event_loop()) as loop:
        return loop.run_until_complete(downloadImages(directory,posts,aiohttp.ClientSession()))
            
@asyncio.coroutine
def downloadImages(directory:str,posts:list, session: aiohttp.ClientSession) -> int:
    #http(s)://i.4cdn.org/[board]/[tim].[file_ext]
    links = []
    for post in posts:
        if post.filename != "":
            l = ("https://i.4cdn.org/" +post.board +"/" + str(post.tim) + post.file_ext,post.filename + post.file_ext)
            links.append(l)

    downloads = [downloadFile(session,url[0],url[1]) for url in links if not os.path.isfile(directory + "/" + url[1])]
    
    for download in asyncio.as_completed(downloads):
        image = yield from download
        file_path = directory + "/" + image[0]

        try:
            with open(file_path,'wb') as fd:
                print("{} saved".format(file_path))
                fd.write(image[1])
        except IOError as e:
            return 1
    return 0

async def downloadFile(session:aiohttp.ClientSession,url:str,file_name:str) -> tuple:
    async with session.get(url) as response:
        return file_name, await response.read()