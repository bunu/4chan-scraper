#!/usr/bin/env python3
import sys,argparse,os,errno,downloader
from thread import Thread

def downloadHelp(loc,posts):
    if(downloader.downloadFiles(loc,posts) == 1):
        print("Error","Ran out of storage at save location")
    else:
        print("Done saving images from {} into {}".format(thread.getThreadName(),results.dir))

parser = argparse.ArgumentParser(add_help=True)
parser.add_argument('-d', '--directory', action='store',default=os.path.abspath(os.path.expanduser('~/Downloads')),dest='dir',help='Location images will be saved')
requiredArgs= parser.add_argument_group('required arguments')
requiredArgs.add_argument('-l', '--link', action='store',required=True,default='',dest='link',help='4chan thread url to scrape from')
results = parser.parse_args()

thread = Thread()
thread.setURL(results.link)
posts = thread.getPosts()

if results.dir.endswith("/"):
    results.dir = results.dir + thread.getThreadName()
else:
    results.dir = results.dir + "/" + thread.getThreadName()

try:
    os.makedirs(results.dir)
    print("Folder created at {}".format(results.dir))
    downloadHelp(results.dir,posts)
except FileExistsError:
    print("Folder already exists at {}. Saving files to it".format(results.dir))
    downloadHelp(results.dir,posts) 
except PermissionError:
    print("Error","Don't have permission to save files to results.diration")
except OSError as e:
    if e.errno != errno.EEXIST:
        raise
    else:
        print("Folder already exists at {}. Saving files to it".format(results.dir))
        downloadHelp(results.dir,posts)