# Purpose
Got tired of having to compile my old Qt version of this program everytime I change distros or use my Windows installation. Decided to just rewrite it in Python 3 so I can have a portable and easy to setup 4chan image scraper.

# Requirements
* Python 3
* [aiohttp](https://aiohttp.readthedocs.io/en/stable/)
* [TkInter](https://wiki.python.org/moin/TkInter)

# Optional dependencies
These aren't required but will result in faster downloads
* [cchardet](https://pypi.python.org/pypi/cchardet/2.1.1)
* [aiodns](https://pypi.python.org/pypi/aiodns)

Everything except TkInter can be installed using pip. Follow the instructions for your OS to install TkInter.


# Features I might add if I feel like it:
* progress bar for the downloads. Figured it out in the Qt version I made. Need to learn how TkInter does it
* add CLI version of this

# Screenshots
Yes. I know it is ugly. Not trying to win any "Prettiest UI" awards. Goal of this is to be straightfoward and easy to use. Might work on the UI to make it look better if I am bored.

![alt text](https://media.discordapp.net/attachments/224644073795878913/426245919382700033/unknown.png)

![alt text](https://media.discordapp.net/attachments/224644073795878913/426246063955902474/unknown.png)

![alt text](https://media.discordapp.net/attachments/224644073795878913/426246192205266946/unknown.png)

![alt text](https://media.discordapp.net/attachments/224644073795878913/426246477887832074/unknown.png)
